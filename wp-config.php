<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'civils' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'P!]jDe16*]dWpB=2*njzwADdT1{7[~;T8,eaV{c_W_Fd_L<]0aCm7Qzs;{9-i#~7' );
define( 'SECURE_AUTH_KEY',  'pqc-Ni5S1ZGG/G#*%@UdL{>eKfVhGHxn:_=[Rg(v(t-<Z!(<49`8R#MxftgJ-_La' );
define( 'LOGGED_IN_KEY',    'k-TDA;XENvStNJqY3/8. 5)I+fgx# L2.0r!%)Upjbi%bbZc[*>~T}1p8J;@,#iQ' );
define( 'NONCE_KEY',        '.jutx>?T!xZw:9U<{N3@A]>K<g94fYiXDdAI_W)MZ3AJ1MUD2]M?PzzzX(x~JG_4' );
define( 'AUTH_SALT',        '@/Jwcj=W#~5?[.D$auuyb3Mo<n.yW2aG/HriK1Si|(nI@Z.5fj{HpG]y|PH*[ACu' );
define( 'SECURE_AUTH_SALT', ')0[_}PYev>~@.iD/^)@Oq<K2<_w0=e~EKWd}/<p!Em`yoYk0iozXn)-4$}0>-1Jr' );
define( 'LOGGED_IN_SALT',   '~qUvvfrfg@kLW3ZgD[G.]EXXe;[6&b?A=zaC`X=_MNYMamgT)Z+qEZj(`OM_l[6L' );
define( 'NONCE_SALT',       'NxVeJr)}T,#W5uC2(ZEuK9g[l!P^)32}21V?3Eq<4N3fuD1KpGH@Y-cJFSa(zP[ ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
