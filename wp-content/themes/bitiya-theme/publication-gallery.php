<?php
/**
 * Template Name: Publication Gllery Template
 *
 */
 get_header();?>
 
 <section class="slider">
	<div class="home_slider owl-carousel">
		<div class="item">
			<img src="<?php bloginfo('template_url'); ?>/images/slide1.png" alt="">		</div>
		<!-- item -->

		<div class="item">
			<img src="<?php bloginfo('template_url'); ?>/images/slide2.png" alt="">		</div>
		<!-- item -->

		<div class="item">
			<img src="<?php bloginfo('template_url'); ?>/images/slide3.png" alt="">		</div>
		<!-- item -->
	</div>
</section>	

<div class="update">
	<div class="upper_up">
		<div class="container">
			<div class="seprate_par">
				<div class="news_iso">
					<div class="content_nw">
						<h2>News Updates</h2>
						<p>Get the latest updates and news ongoing...</p>
					</div>
				</div>
				<!-- iso -->
				
			</div>
			<!-- separate -->
			<div class="news_title">
				<marquee>
					<h3>
						Civil Bank contributes towards relief for earthquake ... Civil Bank Limited has stated that total of 1,889,247 bonus shares
					</h3>
				</marquee>
			</div>
		</div>	
	</div>
	<!-- up -->
	<div class="lower_dw">
		<div class="container">
			<div class="news_text">
				<marquee>
					<p> 
						Civil Bank Limited has stated that total of 1,889,247 bonus shares (7% of paid-up eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.	
					</p>
				</marquee>
			</div>
			<!-- text -->
		</div>
	</div>
	<!-- lower -->
</div>


<section class="offers">
	<div class="s_title">
		<h2>What we offer</h2>
		<span class="design">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 85 10.8" style="enable-background:new 0 0 85 10.8;" xml:space="preserve">
				<style type="text/css">
					.st0{fill:#732978;}
					.st2{fill:#095328;}
				</style>
				<g>
					<path class="st0" d="M36.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C39.6,5.9,37.9,7.2,36.2,8.5z"/>
				</g>
				<g>
					<path class="st2" d="M75.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C78.6,5.9,76.9,7.2,75.2,8.5z"/>
				</g>
			</svg>
		</span>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/auto.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Auto Loan</a>					</div>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/education.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Education Loan</a>					</div>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/brief.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Medical Emergency Loan</a>					</div>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/bag.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Sulav Karja</a>					</div>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/fixed.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Loan Against Fixed Deposit</a>					</div>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/solar.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Solar Karja</a>					</div>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/app.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Consumer Durable Loan</a>					</div>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php bloginfo('template_url'); ?>/images/home.png" alt="">					</div>
					<div class="loan_title">
						<a href="#">Home Loan</a>					</div>
				</div>
			</div>
			<!-- col -->
		</div>
	</div>
</section>
<section class="calc_part">
	<div class="container">
		<div class="row">

			<div class="col-md-6">
				<div class="left_par">
					<div class="design_title">
						<h3>Life and Housing</h3>
					</div>
					<!-- TITLE -->
					<div class="left_con">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 

							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.						</p>
						<div class="plans wow bounceInLeft" style="visibility: visible; animation-duration: 1s;">
							<div class="plan">
								<div class="p_img">
									<img src="<?php bloginfo('template_url'); ?>/images/p-1.png" alt="">								</div>
								<!-- img -->
								<div class="p_cont">
									<h4>Sutkeri Bhatta</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.									</p>
								</div>
							</div>
							<!-- plan -->
							<div class="plan">
								<div class="p_img">
									<img src="<?php bloginfo('template_url'); ?>/images/p-2.png" alt="">								</div>
								<!-- img -->
								<div class="p_cont">
									<h4>Family Planning</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.									</p>
								</div>
							</div>
							<!-- plan -->
						</div>
						<!-- plans -->
					</div>
				</div>
			</div>
			<!-- col -->
			<div class="col-md-6">
				<div class="emi_calc">
					<div class="design_title">
						<h3>EMI Calculator</h3>
					</div>
					<!-- TITLE -->
					<div class="calculator wow bounceInRight" style="visibility: visible; animation-duration: 3s;">
						<form action="" method="get" accept-charset="utf-8">
							<div class="form-group">
								<input type="text" name="" placeholder="Amount of loan">
							</div>
							<!-- froup -->
							<div class="form-group">
								<input type="text" name="" placeholder="Annual Interest Rate">
							</div>
							<!-- froup -->
							<div class="form-group">
								<input type="text" name="" placeholder="Length of loan"> <p>(in years)</p>
							</div>
							<!-- froup -->
							<div class="form-group">
								<input type="text" name="" placeholder="Optional Extra Payment">
							</div>
							<!-- froup -->
							<div class="form-group">
								<input type="text" name="" placeholder="Moratorium Peroid"> <p>(in months)</p>
							</div>
							<!-- froup -->
							<div class="form-group">
								<input type="text" name="" placeholder="Start Date of loan">
							</div>
							<!-- froup -->
							<div class="form-group">
								<label>Round Result</label>
								<input type="checkbox" name="">
								<p>Click here if you want Rs. in round figure.</p>
							</div>
							<!-- froup -->
							<button type="button">Calculate <i class="fa fa-calculator"></i></button>
						</form>
					</div>
				</div>
			</div>
			<!-- col -->
		</div>
	</div>
</section>	

<div class="enquiry">
	<div class="container">
		<div class="t_enquiry">
		<div class="en_icon wow flip" style="animation-duration: 1s; animation-delay: 2s;">
			<img src="<?php bloginfo('template_url'); ?>/images/inquiry.png" alt="">		</div>
			
			<h3>If YOu Have An Inquiry</h3>
			<span class="design">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 85 10.8" style="enable-background:new 0 0 85 10.8;" xml:space="preserve">
				<style type="text/css">
					.st0{fill:#732978;}
					.st2{fill:#095328;}
				</style>
				<g>
					<path class="st0" d="M36.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C39.6,5.9,37.9,7.2,36.2,8.5z"/>
				</g>
				<g>
					<path class="st2" d="M75.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C78.6,5.9,76.9,7.2,75.2,8.5z"/>
				</g>
			</svg>
		</span>		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="en_part">
					<h4>Toll Free Number</h4>
					<a href="javascriptvoi(0)" title="">16600125252</a>				</div>
			</div>
			<!-- col -->

			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="en_part">
					<h4>Follow us on</h4>
					<ul class="socio">
						<li><a href="#" title=""><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt=""></a></li>
						<li><a href="#" title=""><img src="<?php bloginfo('template_url'); ?>/images/tweet.png" alt=""></a></li>
					</ul>
				</div>
			</div>
			<!-- col -->
		</div>
	</div>
</div>

<?php get_footer(); ?>