<?php
/**
 * Template Name:  Videos Template
 *
 */
 get_header();?>
 
  <?php

		// Start the loop.

		while ( have_posts() ) : the_post(); 
      //  $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		?>

        <div class="wrap_crumb_b">
        
	     <ol class="breadcrumb breadcrumb-arrow" style="margin-bottom: 5px;">
         <li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
		 <li><a >Publications & Gallery</a></li>
		<li class="active-d"><?php the_title(); ?></li>
		</ol>
	

</div>

        <?php endwhile; ?>

  
 <div class="col-md-12">
  <section class="basanta-info">
	<div class="container">
		<h2> </h2>
		<div class="row">
		<?php

				 $args = array(

										  'post_type' => 'video',
										  'order' =>'ASC'
										  );
										 $the_query = new WP_Query($args); ?>
                                         <?php if ( $the_query->have_posts() ) : ?>

										  <?php 

										  $c = 1;
                                       while ( $the_query->have_posts() ) : $the_query->the_post();
                                      
									   ?> 	
			
			<div class="col-md-4 video">
				<div class="iframe-video">
					<?php the_content(); ?>
					<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
				</div>
			</div>  
			
			  
			
		<?php endwhile; ?>
			<?php else : ?>
 		<div class="col-md-4">
		<h3>Comming Soon !</h3>
		</div>
 	<?php endif; ?>
            
	</div>
</div>
</section>
</div>
 
 <?php get_footer(); ?>