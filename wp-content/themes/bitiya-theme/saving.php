<?php
/**
 * Template Name: Saving Template
 *
 */
 get_header();?>
 
<?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>
		
 <div class="inner_banner">
     <?php 
					$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                    //print_r($feat_image);die;
					?>
	<img src="<?php echo $feat_image; ?>" alt="">
</div>

<div class="update">
	<div class="upper_up">
		<div class="container">
			<div class="seprate_par">
				<div class="news_iso">
					<div class="content_nw">
						<h2>News Updates</h2>
						<p>Get the latest updates and news ongoing...</p>
					</div>
				</div>
				<!-- iso -->
				
			</div>
			<!-- separate -->
			<div class="news_title">
				<marquee>
				
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				
					
				</marquee>
			</div>
		</div>	
	</div>
	<!-- up -->
	<div class="lower_dw">
		<div class="container">
			<div class="news_text">
				<marquee>
							<?php dynamic_sidebar( 'sidebar-2' ); ?>
						
				
				</marquee>
			</div>
			<!-- text -->
		</div>
	</div>
	<!-- lower -->
</div>

<div class="page-content">
	<div class="page_wrap">
		<div class="container">
			<div class="saving_pge">
				<?php the_content(); ?>
				<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
				<!-- text -->

				<div class="tab_part">
					<div class="row">
						<div class="col-md-3 col-sm-4">
						<?php if( have_rows('saving_account') ): ?>
							<ul class="nav nav-tabs" role="tablist">
							 <?php 
							 $c = 1;
							 while( have_rows('saving_account') ): the_row(); ?>
							 <?php $title = get_sub_field('title_'); 
							       $content = get_sub_field('content');
							  ?>
							 <?php if($c == 1) { ?>
								<li role="presentation" class="active"><a href="#loan_<?php echo $c; ?>" aria-controls="loan_<?php echo $c; ?>" role="tab" data-toggle="tab"><?php echo $title; ?> </a></li>
								<?php }else {?>
								<li role="presentation"><a href="#loan_<?php echo $c; ?>" aria-controls="loan_<?php echo $c; ?>" role="tab" data-toggle="tab"><?php echo $title; ?></a></li>
								
								<?php } ?>
								<?php $c++; endwhile; ?>
								
								
							</ul>
							<?php endif; ?>
						</div>
						<!-- col -->
						<div class="col-md-9 col-sm-8">
							<div class="tab-content">
								<?php if( have_rows('saving_account') ): ?>
								<?php 
							    $c = 1;
							    while( have_rows('saving_account') ): the_row(); ?>
								<?php $title = get_sub_field('title_'); 
							       $content = get_sub_field('content');
							  ?>
							  <?php if($c == '1'){ ?>
								<div role="tabpanel" class="tab-pane active" id="loan_<?php echo $c; ?>">
									<h3>
										<?php echo $title; ?>
									</h3>
									<?php echo $content; ?>
									<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>

								</div>
								<?php } else {?>
								<div role="tabpane<?php echo $c; ?>" class="tab-pane" id="loan_<?php echo $c; ?>">
									<h3>
										<?php echo $title; ?>
									</h3>
									<?php echo $content; ?>
									<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>

								</div>
								<?php }?>
								<?php $c++; endwhile; endif; ?>
								
								
								
							</div>

						</div>
						<!-- col -->
					</div>
				</div>
				<!-- part -->
			</div>
			<!-- page -->
		</div>


		<div class="additonal_services">
			<div class="wrap_services clearfix">
			<?php
										   $args = array(
										  'post_type' => 'additional_services',
										  
										 'order' =>'ASC'
										  );
										 $the_query = new WP_Query($args); ?>
										  <?php if ( $the_query->have_posts() ) : ?>
										  <?php 
										  $c = 1;
									  while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
									    <?php 
					$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                    //print_r($feat_image);die;
					?>
			
				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php  echo $feat_image;?>" alt="">
					</div>
					<div class="desc_ad">
						<h3>
						<?php the_title(); ?>
						</h3>
						<?php the_content(); ?>
						<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
					</div>
				</div>
				
				<?php endwhile;endif; ?>
	

				
			</div>
		</div>
		<!-- services -->
	</div>
	<!-- weap -->
</div>

 <?php endwhile;?>  
<?php get_footer(); ?>