jQuery(document).ready(function($) {

	$('.home_slider').owlCarousel({
		loop: true,
		margin: 0,
		navigation: true,
		pagination: true,
		smartSpeed: 450,
		lazyLoad: true,
		navText: [ "", "" ],
		autoplay: true,
		autoplayHoverPause: true,
		autoplayTimeout: 4000,
		autoplayspeed: 1000,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			1000: {
				items: 1
			}
		},
	}),
		$('.testimonial-slider').owlCarousel({
			loop: true,
			margin: 0,
			navigation: true,
			pagination: true,
			smartSpeed: 450,
			lazyLoad: true,
			navText: [ "", "" ],
			autoplay: true,
			autoplayHoverPause: true,
			autoplayTimeout: 4000,
			autoplayspeed: 1000,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				1000: {
					items: 1
				}
			},
		})

});

$(window).scroll(function() {
	if ($(this).scrollTop() > 1) {
		$('header').addClass("sticky");
	}
	else {
		$('header').removeClass("sticky");
	}

});

jQuery(document).ready(function() {
	new WOW().init();
});

$(document).ready(function() {
	$('#home_modal0').modal('show');
	$('#home_modal1').modal('show');
	$('#home_modal2').modal('show');
	$('#home_modal3').modal('show');
	$('#home_modal4').modal('show');
});
