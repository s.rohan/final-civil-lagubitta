
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tej Solution Pvt. Ltd.</title>

	<!-- Bootstrap core CSS -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/owl.carousel.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
</head>
<body>
	<header id="header" class="">
		<img src="images/header.jpg" alt="">
	</header><!-- /header -->
	<div class="product-content">
		<div class="container">
			<div class="top-product-part">
				<div class="product-title">
					<h2>International Sustainability School</h2>
					<em>
						“Ever envisioned trekking in the Himalayas, living with communities, learning from experts,meeting change makers, working with local youth while being accompanied by like-minded friends living in different countries? All this happens at Sustainability School!”
					</em>
				</div>
				<div class="owl-carousel product-slider ">
					<div class="item">
						<img src="http://www.onceinlife.org/wp-content/uploads/bfi_thumb/DSCF9188-mi5r9evnkb33wqt1dcyyzkvzn876p9u6hski0x3onc.jpg" alt="">
						<div class="product-caption">
							<p>
								International Sustainability School revolves around 10 days experience designed to give you new skills, friends, ideas and inspiration. It is an opportunity to see, feel and know more about sustainability. We give Sustainability Fellows an opportunity to experience grassroot realities as well as learn concrete skills from experts while also discovering themselves. This is a physically, mentally and socially engaging program where participants enrich themselves, the communities they visit and each other.
							</p>
						</div>
					</div>	

					<div class="item">
						<img src="http://www.onceinlife.org/wp-content/uploads/bfi_thumb/DSCF9227-mi5r9dxtdh1tl4ueiukcf34j1ubthkqg5nx0jn52tk.jpg" alt="">
						<div class="product-caption">
							<p>
								International Sustainability School revolves around 10 days experience designed to give you new skills, friends, ideas and inspiration. It is an opportunity to see, feel and know more about sustainability. We give Sustainability Fellows an opportunity to experience grassroot realities as well as learn concrete skills from experts while also discovering themselves. This is a physically, mentally and socially engaging program where participants enrich themselves, the communities they visit and each other.
							</p>
						</div>
					</div>	

					<div class="item">
						<img src="http://www.onceinlife.org/wp-content/uploads/bfi_thumb/DSCF9188-mi5r9evnkb33wqt1dcyyzkvzn876p9u6hski0x3onc.jpg" alt="">
						<div class="product-caption">
							<p>
								International Sustainability School revolves around 10 days experience designed to give you new skills, friends, ideas and inspiration. It is an opportunity to see, feel and know more about sustainability. We give Sustainability Fellows an opportunity to experience grassroot realities as well as learn concrete skills from experts while also discovering themselves. This is a physically, mentally and socially engaging program where participants enrich themselves, the communities they visit and each other.
							</p>
						</div>
					</div>

					<div class="item">
						<img src="http://www.onceinlife.org/wp-content/uploads/bfi_thumb/DSCF9227-mi5r9dxtdh1tl4ueiukcf34j1ubthkqg5nx0jn52tk.jpg" alt="">
						<div class="product-caption">
							<p>
								International Sustainability School revolves around 10 days experience designed to give you new skills, friends, ideas and inspiration. It is an opportunity to see, feel and know more about sustainability. We give Sustainability Fellows an opportunity to experience grassroot realities as well as learn concrete skills from experts while also discovering themselves. This is a physically, mentally and socially engaging program where participants enrich themselves, the communities they visit and each other.
							</p>
						</div>
					</div>	
				</div>


			</div>
			<!-- top -->
			<div class="product-bottom">
				<div class="row">
					<div class="col-md-5">
						<div class="program-details">
							<h2>Program Details</h2>
							<div class="p-detail-wrap">
								<div class="details-block">
									<div class="p-icon">
										<i class="fa fa-calendar"></i>
									</div>
									<div class="date-info">
										<h4>Program Date</h4>
										<p>3-12th June 2017</p>
									</div>
								</div>
								<!-- 1 -->

								<div class="details-block">
									<div class="p-icon">
										<i class="fa fa-clock-o"></i>
									</div>
									<div class="date-info">
										<h4>Application Deadline</h4>
										<p>Applications will be accepted and reviewed on a rolling basis until 7th April, 2017</p>
									</div>
								</div>
								<!-- 1 -->
								<div class="details-block">
									<div class="p-icon">
										<i class="fa fa-tags"></i>
									</div>
									<div class="date-info">
									<h4>Program Fee </h4>
										<p>USD 700</p>
									</div>
								</div>
								<!-- 1 -->

							</div>
							<!-- details -->
						</div>
					</div>	
					<!-- col	 -->

					<div class="col-md-7">
						<div class="program-apply">
							<div class="apply-info">
								<a class="apply-now">
									Apply Now
								</a>
								<p>
									<a href="#" title="">Click here</a> for further information
								</p>
							</div>
						</div>
						<!-- details -->
					</div>	
					<!-- col	 -->
				</div>
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- product -->

	<footer>
		<img src="images/footer.jpg" alt="">
	</footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/init.js"></script>
</body>
</html>
