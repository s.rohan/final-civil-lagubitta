<div class="enquiry">
	<div class="container">
		<div class="t_enquiry">
		<div class="en_icon wow flip" style="animation-duration: 1s; animation-delay: 2s;">
			<img src="<?php bloginfo('template_url'); ?>/images/inquiry.png" alt="">
		</div>
			
			<h3>If YOu Have An Inquiry</h3>
			<span class="design">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 85 10.8" style="enable-background:new 0 0 85 10.8;" xml:space="preserve">
				<style type="text/css">
					.st0{fill:#732978;}
					.st2{fill:#095328;}
				</style>
				<g>
					<path class="st0" d="M36.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C39.6,5.9,37.9,7.2,36.2,8.5z"/>
				</g>
				<g>
					<path class="st2" d="M75.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C78.6,5.9,76.9,7.2,75.2,8.5z"/>
				</g>
			</svg>
		</span>


		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="en_part">
					<h4>Call Us</h4>
					<a href="javascriptvoi(0)" title=""><?php echo of_get_option('phone');?></a>
				</div>
			</div>
			<!-- col -->

			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="en_part">
					<h4>Follow us on</h4>
					<ul class="socio">
						<li><a href="<?php echo of_get_option('facebook');?>" title=""><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt=""></a></li>
						<li><a href="<?php echo of_get_option('twitter');?>" title=""><img src="<?php bloginfo('template_url'); ?>/images/tweet.png" alt=""></a></li>
					</ul>
				</div>
			</div>
			<!-- col -->
		</div>
	</div>
</div>



<div class="carrer-btn">
	<a href="<?php echo esc_url(home_url('/')); ?>career/" title="">Career  <i class="fa fa-user-circle-o"></i></a>
</div>

<footer>
	<div class="footer-top_overlay">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		viewBox="0 0 1280 137.6" style="enable-background:new 0 0 1280 137.6;" xml:space="preserve">
		<style type="text/css">
			.st0{fill:url(#SVGID_1_);}
			.st1{fill:#852B8B;}
		</style>
		<g>
			<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-16.188" y1="115.5829" x2="1300.8052" y2="115.5829">
				<stop  offset="0" style="stop-color:#5C1361"/>
				<stop  offset="0.5468" style="stop-color:#771F7D"/>
				<stop  offset="1" style="stop-color:#601865"/>
			</linearGradient>
			<path class="st0" d="M1300.8,71.3c0,22.3,0,44.5,0,66.8c-16.4,0.7-32.8,1.3-49.2,2.1c-31.8,1.7-63.5,3.5-95.3,5.3
			c-25.2,1.4-50.4,2.7-75.6,4.1c-37.2,2.1-74.5,4.2-111.7,6.2c-31,1.7-62,3.3-92.9,5c-38,2.1-76,4.2-114.1,6.3
			c-24.6,1.4-49.1,2.7-73.7,4c-32.2,1.8-64.5,3.6-96.7,5.4c-30.5,1.7-61,3.3-91.5,5c-31.9,1.8-63.8,3.6-95.7,5.3
			c-25,1.4-50.1,2.7-75.1,4.1c-37.1,2.1-74.2,4.2-111.2,6.2c-31.1,1.7-62.3,3.3-93.4,5c-38.3,2.1-76.7,4.2-115,6.4
			c-7.9,0.5-15.9,1.3-23.8,1.9c-2.9,0.6-1.8-1.6-1.8-2.8c0-16.1,0-32.3,0-48.4c0-12.7,0-25.4,0-38.1c0.6-0.3,1.2-0.5,1.7-0.8
			c56.9-32.5,113.7-65,170.5-97.6c1-0.6,1.8-1.5,2.7-2.3c1.9,0,3.8,0,5.6,0c2.3,0.3,4.6,0.8,6.9,1c26,1.7,51.9,3.4,77.9,5.1
			c31.7,2.1,63.5,4.2,95.2,6.3c25.3,1.7,50.7,3.3,76,5c31.6,2.1,63.2,4.2,94.7,6.3c26.3,1.8,52.5,3.6,78.8,5.3
			c17.8,1.1,35.7,3,53.5,2.9c21.3-0.1,42.6-1.8,63.9-2.9c35.8-1.8,71.7-3.6,107.5-5.3c27.9-1.4,55.7-2.7,83.6-4.1
			c35.4-1.8,70.7-3.5,106.1-5.3c29.3-1.5,58.5-3,87.8-4.4c8.7-0.4,17.9-2.4,26.2-0.5C1182.1,43.2,1241.4,57.4,1300.8,71.3z"/>
		</g>
		<g>
			<path class="st1" d="M-16.2,57.7c58.4-12.4,116.7-24.8,175.1-37.2c0.1,0.2,0.2,0.4,0.3,0.6c-2.7,1.6-5.4,3.3-8.1,4.9
			C97.1,58.8,43,91.6-11.1,124.4c-1.6,1-3.4,1.7-5.1,2.5C-16.2,103.9-16.2,80.8-16.2,57.7z"/>
		</g>
		<g>
			<path class="st1" d="M1304,137.6c-7.6-4.4-15.2-8.7-22.7-13.2c-50.9-30.5-101.8-61-152.7-91.5c-0.9-0.6-1.8-1.1-2.6-2.3
			c9.9,2,19.8,4,29.7,6.1c47.9,10.1,95.7,20.2,143.6,30.2c1.2,0.2,2.5,0.1,3.7,0.1C1303.3,90.5,1303.6,114,1304,137.6z"/>
		</g>
	</svg>
</div>

<div class="footer_content">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="f-title">
					<h4>About</h4>
				</div>
				<p>
					Civil  Laghubitta Bittiya Sanstha Limited, a Subsidiary Company of Civil Bank Limited was incorporated as a public limited company on 1st Chaitra 2067 (15 March 2011) under the Companies Act of Nepal. 
				</p>
			</div>
			<!-- col -->
			<div class="col-md-5 f_line">
				<div class="f-title">
				<h4>Useful Links</h4>
				</div>
				
				<?php 
			   $args = array(
			   'theme_location'  => 'primary',
			   'menu_class'      => 'f-nav',
			   'fallback_cb'     => '',
			  // 'menu_id'         => 'topMain',
			   'menu'    => 'footer-menu',
			  // 'container_id'       => 'cssmenu',
			   //'walker' => new description_walker()
	          // 'container_class' => 'collapse navbar-collapse',
			);
			wp_nav_menu($args); ?>
				
			</div>
			<!-- col -->
			<div class="col-md-4">
				<div class="f-title">
					<h4>Get In Touch</h4>
				</div>
				<p>
				<?php 
				$site_url = home_url( '/' );
				 ?>
					Central Office : <?php echo of_get_option('address');?> <br>
					Phone : <?php echo of_get_option('phone');?><br>
					Email : <a href="mailto:info@civillaghubitta.com.np" target="_top">info@civillaghubitta.com.np</a><br>
					Website : <a target="_blank" href="http://civillaghubitta.com.np/">www.civillaghubitta.com.np</a><br>
				</p>
			</div>
			<!-- col -->


		</div>
		<!-- row -->
	</div>
	<!-- container -->
</div>

<div class="copyright">
	<p>@ <?php echo date('Y'); ?> <?php echo $blog_title = get_bloginfo( 'name' ); ?>. All Rights Reserved. </p>
</div>
</footer>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.fancybox.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/wow.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/init.js"></script>
<script src="<?php bloginfo('template_url'); ?>/jquery.accrue.min.js"></script>
<script>
	jQuery(document).ready(function() {
		$(".search_main button").click(function(){
			$(".search_main").addClass("open");
			 $("#btnAddProfile1").hide();
			 $(".search_main input").after('<button  id="btnAddProfile" type="submit" ><i class="fa fa-search"></i></button>');			
			
		});
		$("#btnAddProfile").hide();
	    if ($("#open span").hasClass("open")) {
	         
       
         }

		$(".main-nav li").has("ul").addClass('dropdown');
		
		 $(".dropdown-toggle").click(function(){
           $("li.dropdown").toggleClass("open");
            });

		$("li.dropdown").click(function(event) {
			$(this).toggleClass('open');
		});

	});

	$("a.mobile-menu").click(function () {
		$('.top-nav').toggleClass("open-menu");
		$(this).toggleClass('see');

	});

	$("a.responsive-menu").click(function () {
		$('.main-nav').toggleClass("open-menu");
		$(this).toggleClass('see');

	});

        $(document).ready(function(){

            // set up the amortization schedule calculator

            $(".calculator-amortization").accrue();



        });
</script>
<?php wp_footer(); ?>
</body>
</html>				
