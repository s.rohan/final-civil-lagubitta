<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="in-content">
	<div class="container">
		<div class="row">
		<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>
			<div class="col-md-12">
				<div class="gallery-single">
					<h3>Album Name : <?php the_title(); ?> </h3>
					<br />
					<div class="row">
					
					<?php the_content(); ?>
				<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>

					</div>
					
					
				</div>
				<!-- wrap -->
			</div>		
			<?php endwhile; ?>

			
		</div>
		
	</div>
</div>

<?php get_footer();
