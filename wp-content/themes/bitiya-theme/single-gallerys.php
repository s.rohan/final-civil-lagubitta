<?php get_header(); ?>

<div class="wrap_crumb_b">

	<ol class="breadcrumb breadcrumb-arrow" style="margin-bottom: 5px;">
				<li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
				<li><a href="<?php echo esc_url(home_url('/')); ?>gallery" >Gallery</a></li>
				<li class="active-d"><?php the_title(); ?></li>
			</ol>
</div>

<div class="in-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="gallery-single">
					<h3>Album Name : <?php the_title(); ?> </h3>
					<br />
					<div class="row">
					<?php
					 $gallery_images = get_field('gallery_images');
					// print_r($events_date);die;
					  ?>
					  <?php foreach($gallery_images as $image){ ?>
						<div class="col-md-4 gallery_image">
							<a href="<?php echo $image['url']; ?>" data-fancybox="group" data-caption="Caption #1">
								<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['title']; ?>" />
							</a>
						</div>
					  <?php } ?>	

					</div>
					

					
				</div>
				<!-- wrap -->
			</div>	

			
		</div>
		
	</div>
</div>
<?php get_footer(); ?>