<?php
/**
 * Template Name: Financial Report Template
 *
 */
	global $post;
	get_header();
	?>
		<div class="inner_banner">
			<?php 
				$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
			?>
			<img src="<?php echo $feat_image; ?>" alt="">
		</div>

<div class="page-content">
	<div class="page_wrap">
	
		<div class="container">
			<div class="career_pge">
				<div class="list_career">
					<h2><?php echo esc_html( get_the_title( $post->ID ) ); ?> :</h2>
					<br />
					<div class="row">
						<ul>
							<?php
								$file_pdfX = get_field( 'civillaghubitta_add_reports', $post->ID );
								foreach( $file_pdfX as $report){
									$file_pdf = get_field( 'file_upload', $report->ID );
									$filesize = filesize( get_attached_file( $file_pdf['id'] ) );
									$filesize = size_format( $filesize, 2 );
									$day = get_the_date( 'd' );
									$mn = get_the_date( 'm' );
									$yr = get_the_date( 'Y' );
									// echo '<pre>';
									// print_r( $report );
									// echo '</pre>';
									?>
										<li>
											<div class="wrap_cj">
												<h4><?php esc_html_e( $report->post_title ); ?></h4>
												<div class="row jb_inf bork">
													<div class="col-md-4">
														<i class="fa fa-calendar"></i> <p><?php echo $yr; ?>-<?php echo $mn; ?>-<?php echo $day; ?></p>
													</div> <!-- col -->
													<div class="col-md-4">
														<strong>Size :</strong> <p><?php echo $filesize; ?></p>
													</div> <!-- col -->
													<div class="col-md-4">
														<strong><a target="_blank" href="<?php echo $file_pdf['url'] ?>">View Report</a></strong>
													</div> <!-- col -->
												</div> <!-- inf -->
											</div>
										</li>
									<?php
								}
										
							?> 	
						</ul> <!-- col -->
					</div>
				</div> <!-- career -->
			</div> <!-- page -->
		</div> 
		<div class="additonal_services">
			<div class="wrap_services clearfix">
				<?php
					$args = array(
								'post_type' => 'additional_services',
								'order' =>'ASC'
							);
					$the_query = new WP_Query($args);
					if ( $the_query->have_posts() ) : 
						$c = 1;
						while ( $the_query->have_posts() ) : $the_query->the_post();
							$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							$link = get_field('links');
				?>
							<div class="service_ad">
								<div class="icon_ad">
									<a href="<?php echo $link; ?>"><img src="<?php  echo $feat_image;?>" alt="<?php the_title(); ?>"></a>
								</div>
								<div class="desc_ad">
									<h3><?php the_title(); ?></h3>
									<?php the_content(); ?>
									<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
								</div>
							</div>
				<?php
						endwhile;
					endif;
				?>
			</div>
		</div> <!-- services -->
	</div> <!-- weap -->
</div>
<?php
	get_footer();