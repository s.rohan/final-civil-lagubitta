<?php
/**
 * Template Name:  Curriculums Template
 *
 */
 get_header();?>
 
<div class="inner_banner">
	<img src="<?php bloginfo('template_url'); ?>/images/loan.png" alt="">
</div>




<div class="page-content">
	<div class="page_wrap">
		<div class="container">
			<div class="saving_pge">
				<h2>Curriculums</h2>
				<div class="small_text">
					<p>
						A certain nominal amount is available for the member clients as the emergency loan. Member clients can apply for this loan in the case of emergency aroused due to natural disaster, accident, serious illness and other small basic needs. It is generally provided to fulfill the small, basic and emergency needs of the member clients.

					</p>
				</div>
				<!-- text -->

				<div class="tab_part">
					<div class="row">
						<div class="col-md-3 col-sm-4">
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#loan_1" aria-controls="loan_1" role="tab" data-toggle="tab">Micro Loan </a></li>
								<li role="presentation"><a href="#loan_2" aria-controls="loan_2" role="tab" data-toggle="tab">Seasonal Loan</a></li>
								<li role="presentation"><a href="#loan_3" aria-controls="loan_3" role="tab" data-toggle="tab">Emergency Loan</a></li>
								<li role="presentation"><a href="#loan_4" aria-controls="loan_4" role="tab" data-toggle="tab">Micro Enterprise Loan</a></li>
								<li role="presentation"><a href="#loan_5" aria-controls="loan_5" role="tab" data-toggle="tab">Diaster Loan</a></li>
							</ul>
						</div>
						<!-- col -->
						<div class="col-md-9 col-sm-8">
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="loan_1">
									<h3>
										Group Saving
									</h3>
									<p>
										Group saving is a mandatory saving. It is a saving product designed to promote saving habit of the member clients. A specified amount has to be deposited by the member clients in their every center meetings as a group saving.

									</p>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>

								</div>
								<div role="tabpanel" class="tab-pane" id="loan_2">
									<h3>
										Pension Saving
									</h3>
									<p>
										Pension Saving is also one of the mandatory savings. Member Clients are supposed to deposit certain amount as their pension saving in their every center meeting. Member Clients are offered to have doubled amount of their deposit under this saving product after the completion of 15 years of their savings. The interest of this doubled amount will be provided to the member clients as the pension or the member clients can withdraw the whole amount. This product basically helps to have certain amount of saving to the member clients at the time of their dependable age.

									</p>
									<p>
										The interest of this doubled amount will be provided to the member clients as the pension or the member clients can withdraw the whole amount. This product basically helps to have certain amount of saving to the member clients at the time of their dependable age.

									</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="loan_3">
									<h3>
										Individual Saving
									</h3>
									<p>
										Individual saving is the voluntary saving product offered to all the member clients. Any amount can be deposited under this saving product by the member clients. This product helps the member clients to improve their saving habit and can withdraw the amount in any center meetings.


									</p>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="loan_4">
									<h3>
										Center Fund Saving
									</h3>
									<p>
										Center Fund Saving is the Mandatory Saving. As per the requirement, 5% amount of the micro loan disbursed to the member clients is deposited as the Center Fund Saving for every individual member clients. It is designed to secure the future of the member clients. Through this saving the member clients can have certain amount at the time of their membership withdraws in the future.


									</p>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="loan_5">
									<h3>
										Welfare Fund Saving
									</h3>
									<p>
										Welfare Fund Savings is a voluntary saving product. Under this savings, the fines, penalties and other voluntary amounts of the center can be deposited by the member clients. It is the saving account of the center not the individual clients. Amount from this saving can be withdrawal at the center meetings. The amount of this saving can be spend on the purchase of center needed material such as mat for sitting, building center house, maintenance of center house, for social activities etc.


									</p>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>
								</div>
							</div>

						</div>
						<!-- col -->
					</div>
				</div>
				<!-- part -->
			</div>
			<!-- page -->
		</div>


		<div class="additonal_services">
			<div class="wrap_services clearfix">
				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/calc.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							EMI Calculator
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->

				

				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/interest.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							Interest Rate
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->

				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/atm.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							ATMs and Branches
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->

				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/calc.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							EMI Calculator
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->
			</div>
		</div>
		<!-- services -->
	</div>
	<!-- weap -->
</div>

<?php get_footer(); ?>