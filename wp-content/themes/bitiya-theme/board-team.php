<?php

/**

 * Template Name:  Board Team

 *

 */

get_header(); ?>



<?php

// Start the loop.

while (have_posts()) : the_post();
	$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	?>

	<div class="wrap_crumb_b">
		<ol class="breadcrumb breadcrumb-arrow" style="margin-bottom: 5px;">
			<li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
			<li class="active-d"><?php the_title(); ?></li>
		</ol>

	</div>

<?php endwhile; ?>



<div class="page-content">

	<div class="page_wrap">

		<div class="container">

			<div class="team_pge">
				<div class="team_sp">
					<h2><?php the_title(); ?></h2>
					<br> <br>
				</div>

				<?php

				$args = array(

					'post_type' => 'staffs',
					'meta_key'     => 'my_key',
					'meta_value'   => 'value1', // change to how "event date" is stored
					'meta_compare' => '',



					'order' => 'ASC'

				);

				$the_query = new WP_Query($args); ?>

				<?php if ($the_query->have_posts()) : ?>

					<?php

					$c = 1;



					while ($the_query->have_posts()) : $the_query->the_post();

						$designation = get_field('designation');
						$additional_info = get_field('additional_info');

						$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

						?>

						<?php if ($designation == 'Chairman') {



							?>

							<div class="main_cm">

								<div class="team_wrap">

									<div class="team_person">

										<img src="<?php echo $feat_image; ?>" alt="" />

									</div>

									<div class="te_name">

										<h4><?php the_title(); ?></h4>

										<p><?php echo $designation; ?></p>
										<div class="st_details">
											<?php echo $additional_info; ?>
										</div>

									</div>

								</div>

								<!-- wrap -->

							</div>

						<?php } ?>

					<?php endwhile;
				endif;   ?>





				<div class="row">

					<?php

					$args = array(

						'post_type' => 'staffs',
						'meta_key'     => 'my_key',
						'meta_value'   => 'value1', // change to how "event date" is stored
						'meta_compare' => '',



						'order' => 'ASC'

					);

					$the_query = new WP_Query($args); ?>

					<?php if ($the_query->have_posts()) : ?>

						<?php

						$c = 1;



						while ($the_query->have_posts()) : $the_query->the_post();

							$designation = get_field('designation');
							$additional_info = get_field('additional_info');

							$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

							?>

							<?php if ($designation !== 'Chairman') { ?>

								<div class="col-md-4 col-sm-6">

									<div class="team_wrap">

										<div class="team_person">

											<img src="<?php echo $feat_image; ?>" alt="" />

										</div>

										<div class="te_name">

											<h4><?php the_title(); ?></h4>

											<p><?php echo $designation; ?></p>
											<div class="st_details">
												<?php echo $additional_info; ?>
											</div>



										</div>

									</div>

									<!-- wrap -->



								</div>

								<!-- col -->

							<?php } ?>

						<?php endwhile;
					endif; ?>

				</div>





			</div>

			<!-- page -->





		</div>

		<!-- conteiner -->



	</div>

	<!-- weap -->

</div>



<?php get_footer(); ?>