<?php

/**
 * Template Name: Home Template
 *
 */
get_header();

?>
<section class="slider">
	<div class="home_slider owl-carousel">

		<?php


		$args = array(
			'post_type' => 'banner',

			'order' => 'ASC'
		);
		$the_query = new WP_Query($args); ?>
		<?php if ($the_query->have_posts()) : ?>
			<?php
			$c = 1;
			while ($the_query->have_posts()) : $the_query->the_post(); ?>
				<?php

				$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
				//print_r($feat_image);die;
				?>
				<div class="item">
					<img src="<?php echo $feat_image ?>" alt=""> </div>
				<!-- item -->
			<?php endwhile;
		endif; ?>

	</div>
</section>
<?php //dynamic_sidebar( 'sidebar-1' );
//print_r($sidebars_widgets('sidebar-1'));die;
?>

<div class="update">
	<div class="upper_up">
		<div class="container">
			<div class="seprate_par">
				<div class="news_iso">
					<div class="content_nw">
						<h2>News Updates</h2>
						<p>Get the latest updates and news ongoing...</p>
					</div>
				</div>
				<!-- iso -->

			</div>
			<!-- separate -->
			<div class="news_title">
				<marquee>

					<?php dynamic_sidebar('sidebar-1'); ?>


				</marquee>
			</div>
		</div>
	</div>
	<!-- up -->
	<div class="lower_dw">
		<div class="container">
			<div class="news_text">
				<marquee>

					<?php dynamic_sidebar('sidebar-2'); ?>


				</marquee>
			</div>
			<!-- text -->
		</div>
	</div>
	<!-- lower -->
</div>


<section class="offers">
	<div class="s_title">
		<h2>What we offer</h2>
		<span class="design">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 85 10.8" style="enable-background:new 0 0 85 10.8;" xml:space="preserve">
				<style type="text/css">
					.st0 {
						fill: #732978;
					}

					.st2 {
						fill: #095328;
					}
				</style>
				<g>
					<path class="st0" d="M36.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C39.6,5.9,37.9,7.2,36.2,8.5z" />
				</g>
				<g>
					<path class="st2" d="M75.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C78.6,5.9,76.9,7.2,75.2,8.5z" />
				</g>
			</svg>
		</span>
	</div>
	<div class="container">
		<div class="row">
			<?php
			$args = array(
				'post_type' => 'loan',

				'order' => 'ASC'
			);
			$the_query = new WP_Query($args); ?>
			<?php if ($the_query->have_posts()) : ?>
				<?php
				$c = 1;
				while ($the_query->have_posts()) : $the_query->the_post(); ?>
					<?php
					$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
					//print_r($feat_image);die;
					$page_link = get_field( 'page_link', $post->ID );
					?>

					<div class="col-md-3 col-sm-4 col-xs-12">
						<div class="loan-wrap">
							<div class="loan_icon">
								<img src="<?php echo $feat_image; ?>" alt=""> </div>
							<div class="loan_title">
								<a href="<?php echo esc_url( $page_link ); ?>"><?php the_title(); ?></a> </div>
						</div>
					</div>

				<?php endwhile;
			endif; ?>
			<!-- col -->


		</div>
	</div>
</section>

<section class="calc_part">
	<div class="container">
		<div class="row">

			
			<!-- col -->
			<div class="col-md-10">
				<div class="emi_calc">
					<div class="design_title">
						<h3>EMI Calculator</h3>
					</div>
					<!-- TITLE -->
					<div class="calculator wow bounceInRight" style="visibility: visible; animation-duration: 3s;">
						<?php //  echo do_shortcode('[emi-calculator]');
						// echo do_shortcode('[emicalc format=”full”][/emicalc]');
						// echo [emicalc format=”sidebar”][/emicalc]
						// echo do_shortcode('[emicalc format="sidebar"][/emicalc]'); 
						//   echo do_shortcode('[emicalc format=”sidebar”][/emicalc]');
						?>


						<div class="calculator-amortization clearfix">
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
</section>

<section class="latest-update">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="home_news">
					<div class="design_title">
						<h3>Latest Updates</h3>
					</div>
					<!-- TITLE -->

					<?php
					$args = array(
						'post_type' => 'news',

						'order' => 'ASC'
					);
					$the_query = new WP_Query($args); ?>
					<?php if ($the_query->have_posts()) { ?>
						<?php
						$c = 1;
						while ($the_query->have_posts()) : $the_query->the_post(); ?>
							<?php
							$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
							//print_r($feat_image);die;
							?>

							<div class="update_col clearfix">
								<?php if ($feat_image) { ?>
									<div class="up_img">
										<img src="<?php echo $feat_image; ?>" alt="">
									</div>
								<?php } ?>
								<!-- img -->

								<div class="up_text">
									<h3><?php the_title(); ?></h3>
									<?php the_excerpt(); ?>
									<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
									<a href="<?php the_permalink(); ?>" title="">Read more <i class="fa fa-long-arrow-right"></i> </a>
								</div>
							</div>

						<?php endwhile;
					} else { ?>
						<p>Comming Soon </p>
					<?php } ?>
					<!-- col -->



					<!-- col -->
				</div>
			</div>
			<!-- col -->

			<div class="col-md-4">
				<div class="useful_link">
					<div class="design_title">
						<h3>Useful links</h3>
					</div>
					<!-- TITLE -->

					<ul>
						<?php
						$args = array(
							'post_type' => 'useful_links',

							'order' => 'ASC'
						);
						$the_query = new WP_Query($args); ?>
						<?php if ($the_query->have_posts()) : ?>
							<?php
							$c = 1;
							while ($the_query->have_posts()) : $the_query->the_post(); ?>
								<?php
								$links = get_field('paste_links_here');
								//print_r($feat_image);die;
								?>
								<li><a href="<?php echo $links; ?>" title=""><?php the_title(); ?></a></li>
								<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>

							<?php endwhile;
						endif; ?>

					</ul>
				</div>
			</div>
			<!-- col -->
		</div>
		<!-- row -->
	</div>
	</script>
</section>



<?php get_footer(); ?>

<!-- Modal -->
<?php
global $wpdb;
$res = $wpdb->query("SELECT post_content from $wpdb->posts where post_type='alerts' and post_status ='publish' order by ID desc limit 0,5");
if ($res > 0) {
	$i = 0;
	$res = $wpdb->last_result;
	foreach ($res as $value) {
		if ($i < 5) {
			?>
			<div class="modal fade" id="<?php echo ('home_modal' . $i) ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<?php
							echo ($value->post_content);
							?>
						</div>
					</div>
				</div>
			</div>
			<?php
			$i++;
		}
	}
}
?>