<?php



get_header();?>



<?php $term_main = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy')); 

      // print_r($term_main->term_id);die;

?>





<div class="wrap_crumb">

	<div class="container">

		<div class="wrap_ins">

			<h2><?php echo $term_main->name; ?></h2>

			<ol class="breadcrumb" style="margin-bottom: 5px;">

				<li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>

				<li><a href="<?php echo esc_url(home_url('/')); ?>branchs">Branch</a></li>

				<li class="active"><?php echo $term_main->name; ?></li>

			</ol>

		</div>

		<!-- ins -->

	</div>	

</div>



<?php

		// Start the loop.

while ( have_posts() ) : the_post(); ?>



<div class="page-content">

	<div class="page_wrap">

		<div class="container">

			<div class="branch_pge">



				<div class="branch_block">

					<div class="t-tle_branch">

						<h4><?php the_title(); ?></h4>

					</div>

					<!-- branch -->

					

					<div class="branch_listing">
						<div class="sn_details">
							

							<?php the_content(); ?>

							<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>

							<?php $address =  get_field('address');

							$contact_no = get_field('contact_no');

							$fax = get_field('fax');

							$email = get_field('email_address'); 

							$maps = get_field('google_maps');

							//$branch_manager = get_field('branch_manager');

							?>

                            
                            <?php if($fax) { ?>

							<h3>Branch Manager Name : <?php echo $fax; ?></h3> 

							<?php } ?>


							<?php if($address) { ?>

							<h3>Address : <?php echo $address; ?></h3> 

							<?php } ?>

							<?php if($contact_no) { ?>

							<h3>Contact no : <?php echo $contact_no; ?></h3> 

							<?php } ?>

							

							<?php if($email) { ?>

							<h3>Email Address  : <?php echo $email; ?></h3> 

							<?php } ?>

						</div>

					</div>

				</div>

				<?php if($maps) { ?>

				<h2 style="margin-bottom:40px;">Find Us at Google maps : </h2>

				<div class="branch_map">

					<?php echo $maps; ?>

				</div>

				

				<?php } ?>

				

			</div>

		</div>

		

		

	</div>

</div>



<?php endwhile; ?>

<?php get_footer(); ?>