<?php
/**
 * Template Name: Branch Template
 *
 */
 get_header();?>

 <?php
		// Start the loop.
		while ( have_posts() ) : the_post(); 
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		?> 
 
<div class="wrap_crumb">
<img src="<?php echo $feat_image; ?>" alt="<?php the_title(); ?>" />
	<div class="container">
		<div class="wrap_ins">
			<h2><?php the_title(); ?></h2>
			<ol class="breadcrumb" style="margin-bottom: 5px;">
				<li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
				<li class="active"><?php the_title(); ?></li>
			</ol>
		</div>
		<!-- ins -->
	</div>	
</div>
 <?php endwhile; ?>
 <?php ?>
 

<div class="update">
	<div class="upper_up">
		<div class="container">
			<div class="seprate_par">
				<div class="news_iso">
					<div class="content_nw">
						<h2>News Updates</h2>
						<p>Get the latest updates and news ongoing...</p>
					</div>
				</div>
				<!-- iso -->
				
			</div>
			<!-- separate -->
			<div class="news_title">
				<marquee>
				
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				
					
				</marquee>
			</div>
		</div>	
	</div>
	<!-- up -->
	<div class="lower_dw">
		<div class="container">
			<div class="news_text">
				<marquee>
							<?php dynamic_sidebar( 'sidebar-2' ); ?>
						
				
				</marquee>
			</div>
			<!-- text -->
		</div>
	</div>
	<!-- lower -->
</div>

<div class="page-content">
	<div class="page_wrap">
		<div class="container">
			<div class="branch_pge">

				<div class="row">
				<?php
				 $args = array(
										 'post_type' => 'branch',
										 'posts_per_page' => -1, 
										  
										 'order' =>'DESC'
										  );
										 $the_query = new WP_Query($args); ?>
										  <?php if ( $the_query->have_posts() ) : ?>
										  <?php 
										  $c = 1;
										
									  while ( $the_query->have_posts() ) : $the_query->the_post();
									 
									
									   ?> 	
				
					<div class="col-md-6">
						<div class="branch_block">
							<div class="t-tle_branch">
								<h4><?php the_title(); ?></h4>
							</div>
							<!-- branch -->
							<div class="branch_listing">
								<?php the_content(); ?>
				<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
							</div>
						</div>
					</div>
					
					 <?php endwhile; endif; ?>
					
				</div>
				

				
			</div>
			<!-- page -->

			
		</div>
		<!-- conteiner -->
		
	</div>
	<!-- weap -->
</div>
 

 
 <?php get_footer(); ?>