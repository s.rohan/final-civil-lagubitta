<?php
/**
 * Template Name:  News and Updates Template
 *
 */
 get_header();?>
 
 <?php
		// Start the loop.
while ( have_posts() ) : the_post(); 
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

<div class="wrap_crumb">
 <img src="<?php echo $feat_image; ?>" alt="<?php the_title(); ?>" />
	<div class="container">
		
		
		<div class="wrap_ins">
			<h2><?php the_title(); ?></h2>
			<ol class="breadcrumb" style="margin-bottom: 5px;">
				<li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
				
				<li class="active"><?php the_title(); ?></li>
			</ol>
		</div>
		<!-- ins -->
	</div>	
</div>

<?php endwhile; ?>
 
<div class="update">
	<div class="upper_up">
		<div class="container">
			<div class="seprate_par">
				<div class="news_iso">
					<div class="content_nw">
						<h2>News Updates</h2>
						<p>Get the latest updates and news ongoing...</p>
					</div>
				</div>
				<!-- iso -->
				
			</div>
			<!-- separate -->
			<div class="news_title">
				<marquee>
				
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				
					
				</marquee>
			</div>
		</div>	
	</div>
	<!-- up -->
	<div class="lower_dw">
		<div class="container">
			<div class="news_text">
				<marquee>
							<?php dynamic_sidebar( 'sidebar-2' ); ?>
						
				
				</marquee>
			</div>
			<!-- text -->
		</div>
	</div>
	<!-- lower -->
</div>



<div class="page-content">
	<div class="page_wrap">
		<div class="container">
			<div class="news_pge">
			<?php
										   $args = array(
										  'post_type' => 'news',
										  
										 'order' =>'ASC'
										  );
										 $the_query = new WP_Query($args); ?>
										  <?php if ( $the_query->have_posts() ) {?>
										  <?php 
										  $c = 1;
									  while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
                                       <?php 
					$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                    //print_r($feat_image);die;
					?>
				<div class="news_block">
					<div class="row">
					<?php
						  $location = get_field('location');
						  $event_date = explode("-",get_field('event_date')); 
						  $month = date("M",strtotime(get_field('event_date')));
						 // print_r($month);die;
						  //echo date("m");
						  
						
					 ?>
						<div class="col-md-2 col-sm-2">
							<div class="np_date">
								<h2><?php echo $event_date[2]; ?></h2>
								<h4><?php echo $month; ?></h4>
							</div>
						</div>
						<!-- col -->

						<div class="col-md-7 col-sm-7">
							<div class="np_content">
								<h3><?php the_title(); ?></h3>
								<span class="np_place"> <i class="fa fa-map-marker"></i><?php echo $location; ?></span>
								<?php the_content(); ?>
					<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
							</div>
						</div>
						<!-- col -->

						<div class="col-md-3 col-sm-3">
							<div class="np_img">
								<?php	the_post_thumbnail(); ?>
							</div>
						</div>
						<!-- col -->
					</div>
				</div>
				
				<?php endwhile; } else { ?>
				<h2>No News Available. </h2>
				<?php } ?>
			
			</div>
			<!-- page -->

			
		</div>
		<!-- conteiner -->
		
	</div>
	<!-- weap -->
</div>
 
 <?php get_footer(); ?>
