<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

 <?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>
		
 
 
<div class="inner_banner">
     <?php 
					//$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					$feat_image = get_field('banner');
                    //print_r($feat_image);die;
					?>
	<img src="<?php echo $feat_image['url']; ?>" alt="">
</div>


<div class="page-content">
	<div class="page_wrap">
		<div class="container">
			<div class="saving_pge">
				<h2><?php the_title(); ?></h2>
				<div class="small_text">
				<?php the_content(); ?>
				<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
					
				</div>
				<!-- text -->

				
				<!-- part -->
			</div>
			<!-- page -->
		</div>


		<div class="additonal_services">
			<div class="wrap_services clearfix">
				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/calc.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							EMI Calculator
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->

				

				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/interest.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							Interest Rate
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->

				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/atm.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							ATMs and Branches
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->

				<div class="service_ad">
					<div class="icon_ad">
						<img src="<?php bloginfo('template_url'); ?>/images/calc.png" alt="">
					</div>
					<div class="desc_ad">
						<h3>
							EMI Calculator
						</h3>
						<p>
							Explore the power of simpler and smarter banking. Bank online with over 250 services
						</p>
					</div>
				</div>
				<!-- ad -->
			</div>
		</div>
		<!-- services -->
	</div>
	<!-- weap -->
</div>

 <?php endwhile; ?>

<?php get_footer(); ?>
