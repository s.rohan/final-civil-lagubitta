<?php
/**
 * Template Name:  Gllery Template
 *
 */
get_header();?>

<?php
		// Start the loop.
while ( have_posts() ) : the_post(); 
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

         <div class="wrap_crumb_b">
	     <ol class="breadcrumb breadcrumb-arrow" style="margin-bottom: 5px;">
         <li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
		 <li><a >Publications & Gallery</a></li>
		<li class="active-d"><?php the_title(); ?></li>
		</ol>
        </div>

<?php endwhile; ?>



<div class="page-content">
	<div class="page_wrap">
		<div class="container">
			<div class="gallery_pge">
				<div class="row">
					
					<?php
					$args = array(
						'post_type' => 'gallerys',
						
						'order' =>'ASC'
						);
						$the_query = new WP_Query($args); ?>
						<?php if ( $the_query->have_posts() ) : ?>
							<?php 
							$c = 1;
							
							while ( $the_query->have_posts() ) : $the_query->the_post();
							$events_date = explode("-",get_field('events_date'));
							//print_r(get_field('events_date'));die;
							$month = date("M",strtotime(get_field('events_date')));
							$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							
							?> 	
							<div class="col-md-4 col-sm-6">
								<div class="gall_wrap">
									<a title="">
										<div class="g_img">
											<img class="thumbnail img-responsive" id="image-1" src="<?php echo $feat_image; ?>">
										</div>
									</a>
									<div class="g_text">
										<div class="g_title">
											<a  title=""><?php the_title(); ?></a>
										</div>
										<div class="g_details">
											<span class="g_date"><?php echo $month; ?> <?php echo $events_date[2]; ?>, <?php echo $events_date[0]; ?></span>
											<a href="<?php the_permalink(); ?>"> <i class="fa fa-camera-retro"></i> View Gallery</a>
										</div>
									</div>
								</div>
							</div>
							
						<?php endwhile; endif; ?>
					</div>
				</div>
				<!-- page -->
			</div>
		</div>
		<!-- weap -->
	</div>

	
	<?php get_footer(); ?>