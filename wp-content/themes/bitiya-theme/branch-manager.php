<?php

/**

 * Template Name:  Branch Manager

 *

 */

 get_header();?>

 <?php

		// Start the loop.

		while ( have_posts() ) : the_post(); ?> 

     <div class="wrap_crumb">

	<div class="container">

		<div class="wrap_ins">

			<h2><?php the_title(); ?></h2>

			<ol class="breadcrumb" style="margin-bottom: 5px;">

			<li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>

			<li class="active"><?php the_title(); ?></li>

			</ol>

		</div>

		<!-- ins -->

	</div>	

</div>



<?php endwhile; ?>

<div class="update">

	<div class="upper_up">

		<div class="container">

			<div class="seprate_par">

				<div class="news_iso">

					<div class="content_nw">

						<h2>News Updates</h2>

						<p>Get the latest updates and news ongoing...</p>

					</div>

					

				</div>

				<!-- iso -->

				

			</div>

			<!-- separate -->

			<div class="news_title">

				<marquee>

					<?php dynamic_sidebar( 'sidebar-1' ); ?>

				</marquee>

			</div>

		</div>	

	</div>

	<!-- up -->

	<div class="lower_dw">

		<div class="container">

			<div class="news_text">

				<marquee>

						<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</marquee>

			</div>

			<!-- text -->

		</div>

	</div>

	<!-- lower -->

</div>



<div class="page-content">

	<div class="page_wrap">

		<div class="container">

			<div class="branch_manage_pge">

				<div class="row">

					<?php

										   $args = array(

										  'post_type' => 'branch-managers',

										  

										 'order' =>'ASC'

										  );

										 $the_query = new WP_Query($args); ?>

										  <?php if ( $the_query->have_posts() ) : ?>

										  <?php 

										  $c = 1;

									  while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 

                                       <?php 

					$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

                    //print_r($feat_image);die;

					?>

					

					<div class="col-md-6">

						<div class="mang_wrap clearfix">

							<div class="mang_detail">

								<div class="mang_pers">

								

							<?php	the_post_thumbnail(array('321','294')); ?>

								</div>

								<!-- pers -->

								<div class="mag_titl">

									<h4>

										<?php the_title(); ?>

									</h4>

								</div>

								<!-- titl -->

							</div>

							<?php

							 $designation =  get_field('designation');

					      $phone = get_field('phone');

						  $email = get_field('email');

						  $website = get_field('website'); 

						  $head_office = get_field('head_office');

					 ?>

							<div class="mang_official">

								<div class="branch_pla">

									<h3>

									<?php echo $designation; ?>

									</h3>



								</div>

								<div class="official_tex">

									<p>

									<?php if($phone){ ?>

									<strong>Phone: </strong>  <?php echo $phone; ?> <br>

									<?php } ?>

									

									<?php if($email){ ?>

									<strong>Email: </strong>  <?php echo $email; ?><br>

									<?php } ?>

									

									<?php if($website){ ?>

										<strong>Website: </strong> <?php echo $website; ?> <br>

									<?php } ?>

										

									<?php if($head_office){ ?>

										<strong>Address: </strong> <?php echo $head_office; ?>

									<?php } ?>	

									

										

									</p>

									<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>

								</div>

							</div>	

							<!-- official -->

						</div>

					</div>

					

					<?php endwhile; endif; ?>

					<!-- col -->

				</div>

			</div>

			<!-- page -->



			

		</div>

		<!-- conteiner -->



	</div>

	<!-- weap -->

</div>

<?php get_footer(); ?>