<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
 
 class description_walker extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		if (array_search('menu-item-has-children', $item->classes)) {
			$output .= sprintf("\n<li class='dropdown %s'><a href='%s' class=\"dropdown-toggle\" data-toggle=\"dropdown\" >%s <i class='fa fa-caret-down'></i></a>\n", ( array_search('current-menu-item', $item->classes) || array_search('current-page-parent', $item->classes) ) ? 'active' : '', $item->url, $item->title
			);
		} else {
			$output .= sprintf("\n<li %s><a href='%s'>%s</a>\n", ( array_search('current-menu-item', $item->classes) ) ? '' : '', $item->url, $item->title
			);
		}
	}
 
	function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu\" role=\"menu\">\n";
	}
	
	
	
	
}

 
 
 
 
function twentyseventeen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentyseventeen' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );

	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'twentyseventeen' ),
		'social' => __( 'Social Links Menu', 'twentyseventeen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', twentyseventeen_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'twentyseventeen' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'twentyseventeen' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters Twenty Seventeen array of starter content.
	 *
	 * @since Twenty Seventeen 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'twentyseventeen_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'twentyseventeen_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function twentyseventeen_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( twentyseventeen_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );
}
add_action( 'template_redirect', 'twentyseventeen_content_width', 0 );

/**
 * Register custom fonts.
 */
function twentyseventeen_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'twentyseventeen' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentyseventeen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentyseventeen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'News First Row ', 'twentyseventeen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'News Second Row', 'twentyseventeen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	
}
add_action( 'widgets_init', 'twentyseventeen_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function twentyseventeen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'twentyseventeen_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function twentyseventeen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function twentyseventeen_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'twentyseventeen_pingback_header' );

/**
 * Display custom color CSS.
 */
function twentyseventeen_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo twentyseventeen_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'twentyseventeen_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function twentyseventeen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyseventeen-fonts', twentyseventeen_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'twentyseventeen-style', get_stylesheet_uri() );

	// Load the dark colorscheme.
	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'twentyseventeen-style' ), '1.0' );
	}

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'twentyseventeen-style' ), '1.0' );
		wp_style_add_data( 'twentyseventeen-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentyseventeen-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'twentyseventeen-style' ), '1.0' );
	wp_style_add_data( 'twentyseventeen-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentyseventeen-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$twentyseventeen_l10n = array(
		'quote'          => twentyseventeen_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	if ( has_nav_menu( 'top' ) ) {
		wp_enqueue_script( 'twentyseventeen-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
		$twentyseventeen_l10n['expand']         = __( 'Expand child menu', 'twentyseventeen' );
		$twentyseventeen_l10n['collapse']       = __( 'Collapse child menu', 'twentyseventeen' );
		$twentyseventeen_l10n['icon']           = twentyseventeen_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
	}

	wp_enqueue_script( 'twentyseventeen-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.0', true );

	wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/assets/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );

	wp_localize_script( 'twentyseventeen-skip-link-focus-fix', 'twentyseventeenScreenReaderText', $twentyseventeen_l10n );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentyseventeen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function twentyseventeen_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function twentyseventeen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function twentyseventeen_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'twentyseventeen_front_page_template' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );

/*custom post type with taxonomy */
function my_custom_post_branch() {
  $labels = array(
    'name'               => _x( 'Branch', 'post type general name' ),
    'singular_name'      => _x( '', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Branch' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Branch found' ),
    'not_found_in_trash' => __( 'No Branch found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Branch'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Branch',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'branch', $args ); 
}
add_action( 'init', 'my_custom_post_branch' );



function my_custom_post_branch_manager() {
  $labels = array(
    'name'               => _x( 'Branch Manager', 'post type general name' ),
    'singular_name'      => _x( 'Branch', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Branch' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Branch found' ),
    'not_found_in_trash' => __( 'No Branch found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Branch Manager'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Branch Manager',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', '', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
 // register_post_type( 'branch-managers', $args ); 
}
add_action( 'init', 'my_custom_post_branch_manager' );

function my_custom_post_news() {
  $labels = array(
    'name'               => _x( 'News', 'post type general name' ),
    'singular_name'      => _x( 'Branch', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Branch' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Branch found' ),
    'not_found_in_trash' => __( 'No Branch found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'News & Update'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'News',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'news', $args ); 
}
add_action( 'init', 'my_custom_post_news' );


function my_custom_post_staff() {
  $labels = array(
    'name'               => _x( 'Staff', 'post type general name' ),
    'singular_name'      => _x( 'Staff', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Staff' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Staff found' ),
    'not_found_in_trash' => __( 'No Staff found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Staff'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Staff',
    'public'        => true,
    'menu_position' => 5,
	'register_meta_box_cb' => 'add_events_metaboxes',
    'supports'      => array( 'title', '', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'staffs', $args ); 
}
add_action( 'init', 'my_custom_post_staff' );

add_action( 'add_meta_boxes', 'add_events_metaboxes' );

function add_events_metaboxes() {
	add_meta_box('wpt_events_location', 'Staff Type', 'wpt_events_location', 'staffs', 'side', 'default');
}

function wpt_events_location($post) {
        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'wdm_meta_box', 'wdm_meta_box_nonce' );

        /*
         * Use get_post_meta() to retrieve an existing value
         * from the database and use the value for the form.
         */
        $value = get_post_meta( $post->ID, 'my_key', true ); //my_key is a meta_key. Change it to whatever you want
		//print_r($value);die;

        ?>
        <br />  
        <input type="radio" name="the_name_of_the_radio_buttons" value="value1" <?php checked( $value, 'value1' ); ?> >Board of comitteee<br>
		<br>
        <input type="radio" name="the_name_of_the_radio_buttons" value="value2" <?php checked( $value, 'value2' ); ?> >Management team<br>
       

        <?php

}

     function wdm_save_meta_box_data( $post_id ) {
	// print_r($post_id);die;
        /*
         * We need to verify this came from our screen and with proper authorization,
         * because the save_post action can be triggered at other times.
         */

        // Check if our nonce is set.
        if ( !isset( $_POST['wdm_meta_box_nonce'] ) ) {
                return;
        }

        // Verify that the nonce is valid.
        if ( !wp_verify_nonce( $_POST['wdm_meta_box_nonce'], 'wdm_meta_box' ) ) {
                return;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return;
        }

        // Check the user's permissions.
        if ( !current_user_can( 'edit_post', $post_id ) ) {
                return;
        }

        // Sanitize user input.
        $new_meta_value = ( isset( $_POST['the_name_of_the_radio_buttons'] ) ? sanitize_html_class( $_POST['the_name_of_the_radio_buttons'] ) : '' );
       //  print_r($new_meta_value);die;
        // Update the meta field in the database.
        update_post_meta( $post_id, 'my_key', $new_meta_value );


	 
	 }
	 
	 add_action( 'save_post', 'wdm_save_meta_box_data' );
	 
	 

function adfad(){

}


function my_custom_post_banner() {
  $labels = array(
    'name'               => _x( 'Banner', 'post type general name' ),
    'singular_name'      => _x( 'Banner', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Banner' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Banner found' ),
    'not_found_in_trash' => __( 'No Banner found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Banner'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Banner',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', '', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'banner', $args ); 
}
add_action( 'init', 'my_custom_post_banner' );

function my_custom_post_offer() {
  $labels = array(
    'name'               => _x( 'What we offer', 'post type general name' ),
    'singular_name'      => _x( 'What we offer', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All What we offer' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No What we offer found' ),
    'not_found_in_trash' => __( 'No What we offer found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'What we offer'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'What we offer',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'loan', $args ); 
}
add_action( 'init', 'my_custom_post_offer' );

function my_custom_post_life() {
  $labels = array(
    'name'               => _x( 'Life and Housing', 'post type general name' ),
    'singular_name'      => _x( 'Life and Housing', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Life and Housing' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Life and Housing found' ),
    'not_found_in_trash' => __( 'No Life and Housing found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Life and Housing'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Life and Housing',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'life', $args ); 
}
add_action( 'init', 'my_custom_post_life' );


function my_custom_post_additional_service() {
  $labels = array(
    'name'               => _x( 'Additional Services', 'post type general name' ),
    'singular_name'      => _x( 'Additional Services', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Additional Services' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Additional Services found' ),
    'not_found_in_trash' => __( 'No Additional Services found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Additional Services'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Additional Services',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'additional_services', $args ); 
}
add_action( 'init', 'my_custom_post_additional_service' );


function my_custom_post_additional_userful_links() {
  $labels = array(
    'name'               => _x( 'Useful Links', 'post type general name' ),
    'singular_name'      => _x( 'Useful Links', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Useful Links' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Useful Links found' ),
    'not_found_in_trash' => __( 'No Useful Links found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Useful Links'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Useful Links',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', '', '', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'useful_links', $args ); 
}
add_action( 'init', 'my_custom_post_additional_userful_links' );



function my_custom_post_gallery() {
  $labels = array(
    'name'               => _x( 'Gallery ', 'post type general name' ),
    'singular_name'      => _x( 'Gallery ', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Gallery ' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Branch' ),
    'not_found'          => __( 'No Gallery  found' ),
    'not_found_in_trash' => __( 'No Gallery  found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Gallery '
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Gallery',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', '', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'gallerys', $args ); 
}
add_action( 'init', 'my_custom_post_gallery' );


function my_custom_post_career() {
  $labels = array(
    'name'               => _x( 'Jobs list ', 'post type general name' ),
    'singular_name'      => _x( 'Jobs list ', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Jobs list' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Job List' ),
    'not_found'          => __( 'No Jobs list  found' ),
    'not_found_in_trash' => __( 'No Jobs list  found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Career '
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Career',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', '', '', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'job_listing', $args ); 
}
add_action( 'init', 'my_custom_post_career' );

function my_custom_post_financial_report() {
  $labels = array(
    'name'               => _x( 'Financial Report  ', 'post type general name' ),
    'singular_name'      => _x( 'Financial Report  ', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Report ' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Financial Report' ),
    'not_found'          => __( 'No Financial Report   found' ),
    'not_found_in_trash' => __( 'No Financial Report   found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Financial Report '
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Financial Report',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', '', '', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'reports', $args ); 
}
add_action( 'init', 'my_custom_post_financial_report' );

function my_custom_post_financial_video() {
  $labels = array(
    'name'               => _x( 'Videos', 'post type general name' ),
    'singular_name'      => _x( 'Videos', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Videos ' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Financial Report' ),
    'not_found'          => __( 'No Videofound' ),
    'not_found_in_trash' => __( 'No Video  found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Videos'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Videos',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', '', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'video', $args ); 
}
add_action( 'init', 'my_custom_post_financial_video' );


function my_custom_post_financial_brochure() {
  $labels = array(
    'name'               => _x( 'Brochures', 'post type general name' ),
    'singular_name'      => _x( 'Brochures', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Brochures ' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Brochures' ),

    'not_found'          => __( 'No Brochures found' ),
    'not_found_in_trash' => __( 'No Brochures  found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Brochures'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Brochures',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', '', '', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'brochure', $args ); 
}
add_action( 'init', 'my_custom_post_financial_brochure' );














 


function my_custom_alerts() {
  $labels = array(
    'name'               => _x( 'Alerts', 'post type general name' ),
    'singular_name'      => _x( 'Alert', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New ' ),
    'edit_item'          => __( 'Edit ' ),
    'new_item'           => __( 'New ' ),
    'all_items'          => __( 'All Alerts' ),
    'view_item'          => __( 'View ' ),
    'search_items'       => __( 'Search Alert' ),
    'not_found'          => __( 'No Alert found' ),
    'not_found_in_trash' => __( 'No Alert found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Alerts'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Alert',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail', '', '' ),
    'has_archive'   => true,
  );
  register_post_type( 'alerts', $args ); 
}
add_action( 'init', 'my_custom_alerts' );

function hs_image_editor_default_to_gd( $editors ) {
$gd_editor = 'WP_Image_Editor_GD';
$editors = array_diff( $editors, array( $gd_editor ) );
array_unshift( $editors, $gd_editor );
return $editors;
}
add_filter( 'wp_image_editors', 'hs_image_editor_default_to_gd' );