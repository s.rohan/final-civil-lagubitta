<?php
/**
 * Template Name:  Carrer Template
 *
 */
 get_header();?>
 

<div class="page-content avoid_p">
	<div class="page_wrap">
	 <?php
  // Start the loop.
  while ( have_posts() ) : the_post();
  	$content_first = get_field('content_first');
	$content_second_ = get_field('content_second_');
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
   ?>
		 <div class="wrap_crumb_b">
        
	     <ol class="breadcrumb breadcrumb-arrow" style="margin-bottom: 5px;">
         <li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
		<li class="active-d"><?php the_title(); ?></li>
		</ol>
	

</div>
		
	<?php endwhile; ?>
		<div class="container">
			<div class="career_pge">
				<div class="list_career">
					<h3>List of available jobs </h3>
					<div class="row">
						<?php
				 $args = array(
										  'post_type' => 'job_listing',
										  
										 'order' =>'ASC'
										  );
										 $the_query = new WP_Query($args); ?>
										  <?php if ( $the_query->have_posts() ) { ?>
										  <?php 
										  $c = 1;
										
									  while ( $the_query->have_posts() ) : $the_query->the_post();
									   $status = get_field('status');
									   $file_pdf = get_field('file_pdf');
									   $filesize = filesize( get_attached_file($file_pdf['id']) );
									   $filesize = size_format($filesize, 2);
									   //print_r($status);die;
									   $day = get_the_date('d');
			                           $mn = get_the_date('m');
									   $yr = get_the_date('Y');
									
									   ?> 	
						<div class="col-md-6">
							<div class="wrap_cj">
								<h4> <?php  the_title();?>	</h4>
								<div class="row jb_inf">
									<div class="col-md-4">
										<i class="fa fa-calendar"></i> <p><?php echo $yr; ?>-<?php echo $mn; ?>-<?php echo $day; ?></p>
									</div>
									<!-- col -->

									<div class="col-md-4">
										<strong>Size :</strong> <p><?php echo $filesize; ?></p>
									</div>
									<!-- col -->

									<div class="col-md-4">
										<strong>Status :</strong> <p><?php echo $status; ?></p>
									</div>
									<!-- col -->
								</div>
								<!-- inf -->
								<div class="cr_btn">
									<a target="_blank" href="<?php echo $file_pdf['url'] ?>">Download</a>
									<button type="button" data-toggle="modal" data-target="#myModal">Apply Now</button>
								</div>
							</div>
						</div>
						
						 <?php endwhile; } else { ?>
						 <h3>COMMING SOON! </h3>
						 <?php } ?>	 
						<!-- col -->

					</div>
				</div>
				<!-- career -->
			</div>
			<!-- page -->
		</div>
		<!-- conteiner -->
		
	</div>
	<!-- weap -->
</div>



<!-- Modal -->
<div class="modal fade apply-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h2 class="modal-title" id="myModalLabel">Apply Your Form</h2>
			</div>
			<div class="modal-body">
				<div class="apply_form">
				<?php echo do_shortcode('[contact-form-7 id="343" title="Career Form"]'); ?>  
					
				</div>
			</div>
			
		</div>
	</div>
</div>
 
 <?php get_footer(); ?>