<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/x-icon">
	
	<link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" rel="stylesheet">
	<link href="<?php bloginfo('template_url'); ?>/css/jquery.fancybox.min.css" rel="stylesheet">
	<link href="<?php bloginfo('template_url'); ?>/css/animate.css" rel="stylesheet">
	<link href="<?php bloginfo('template_url'); ?>/css/style.css" rel="stylesheet">
	<link href="<?php bloginfo('template_url'); ?>/css/responsive.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	

	<?php wp_head(); ?>

	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104184860-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
	<header id="header" class="">
		<div class="top-header">
			<div class="container">
				<div class="logo">
					<a href="<?php echo esc_url(home_url('/')); ?>" title="">
						<img src="<?php echo of_get_option('logo_image');?>" alt="">	
						</a>
						</div>
				<!-- logo -->
				<div id="open" class="top-nav pull-right">
					<span  class="search_main">
					    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<input type="search" id="myMessage" name="s" placeholder="Search here....">
						</form>
						<button  id="btnAddProfile1" type="button" ><i class="fa fa-search"></i></button>
					</span>	
					
					
					<a class="mobile-menu">
						<span></span>
						<span></span>
						<span></span>					
					</a>					
					
					<?php 
			   $args = array(
			   'theme_location'  => 'primary',
			 //  'menu_class'      => 'main-menu',
			   'fallback_cb'     => '',
			  // 'menu_id'         => 'topMain',
			   'menu'    => 'main-menu',
			  // 'container_id'       => 'cssmenu',
			   //'walker' => new description_walker()
	          // 'container_class' => '',
			  'container' => ''
			);
			wp_nav_menu($args); ?>
					
				</div>
				<!-- nav -->
			</div>
		</div>
		<!-- top-header -->

		<div class="bottom-header">
			<div class="container">
				
				<div class="main-nav pull-right">

					<div class="respnsive-bar pull-right">
						<span>Menu</span>
						<a class="responsive-menu">
							<span></span>
							<span></span>
							<span></span>						</a>					
							</div>
					
					<?php 
			   $args = array(
			   'theme_location'  => 'primary',
			  // 'menu_class'      => 'main-menu2',
			   'fallback_cb'     => '',
			  // 'menu_id'         => 'topMain',
			   'menu'    => 'main-menu2',
			  // 'container_id'       => 'cssmenu',
			   'walker' => new description_walker(),
			   'container' => ''
	          // 'container_class' => 'collapse navbar-collapse',
			);
			wp_nav_menu($args); ?>
			
					
					
					<?php /*?><ul>
					<li><a href="news.php" title="">About Us</a> </li>   
						           
						<!--<li><a href="branch.php" title="">Branch  <span><i class="fa fa-caret-down"></i></span></a> </li> -->       
						<li><a href="news.php" title="">Branch</a> </li>   
						<li><a href="index.php" title="">Publication & Gallery <span><i class="fa fa-caret-down"></i></span></a> 

							<ul class="dropdown-menu">
								<li><a href="gallery.php" title="">Gallery</a></li>
								<li><a href="index.php" title="">Videos</a></li>
								<li><a href="index.php" title="">Brochures</a></li>
							</ul>
						</li>       
						<li><a href="news.php" title="">News and Updates </a> </li>
					</ul><?php */?>
					
				</div>
				<!-- main-nav -->
			</div>
		</div>
		<!-- bottom -->



	</header><!-- /header -->
