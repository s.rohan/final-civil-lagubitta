<?php

/**

 * Template Name: Contact Us Template

 *

 */

 get_header();?>

 <?php

		// Start the loop.

		while ( have_posts() ) : the_post(); 
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		?>

        <div class="wrap_crumb">
         <img src="<?php echo $feat_image; ?>" alt="<?php the_title(); ?>" />

	<div class="container">

		<div class="wrap_ins">

			<h2>Contact Us</h2>

			<ol class="breadcrumb" style="margin-bottom: 5px;">

				<li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>

				<li class="active"><?php the_title(); ?></li>

				

			</ol>

		</div>

		<!-- ins -->

	</div>	

</div>

        <?php endwhile; ?>



        <div class="page-content">

	<div class="page_wrap">

		<div class="container">

			<div class="contact_pge">

				<div class="row">	

					<div class="col-md-6"> 

						<div class="head_cp">

							<h2>	 CONTACT INFO</h2>

							<p>	Welcome to our Website. We are glad to have you around.	</p>

						</div>

						

						<ul	class="cp_details">

							<li>

								<span><i class="fa fa-phone"> </i>	</span>

								<div class="ov_text">

									<h4>Phone :</h4>

									<p> <?php echo of_get_option('phone');?></p>



								</div>

							</li>



							<li>

								<span><i class="fa fa-envelope"> </i>	</span>

								<div class="ov_text">

									<h4>Email :</h4>

									<p>

									<a href="mailto:<?php echo of_get_option('email');?>?Subject=Hello%20again" target="_top"><?php echo of_get_option('email');?></a>

									</p>

									

								</div>

							</li>



							<li>

								<span><i class="fa fa-map-marker"> </i>	</span>

								<div class="ov_text">

									<h4>Location :</h4>

									<p><?php echo of_get_option('address');?></p>

									

								</div>

							</li>

						</ul>

					</div>

					<!-- col	 -->

					<div class="col-md-6">

						<div class="cp_form">

							<div class="head_cp">

								<h2>SEND A MESSAGE</h2>

								<p>Your email address will not be published. Required fields are marked.</p>

							</div>

							<?php echo do_shortcode('[contact-form-7 id="172" title="Contact Us"]'); ?>  

							

						</div>

					</div>

					<!-- col -->

				</div>	



				<div class="cp_location">

					<?php // echo of_get_option('googlemaps');?>

				</div>

			</div>

			<!-- page -->

		</div>





	</div>

	<!-- weap -->

</div>

 



<?php get_footer(); ?>