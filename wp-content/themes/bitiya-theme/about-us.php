<?php
/**
 * Template Name: About Us Template
 *
 */
 get_header();?>
 
  <?php

		// Start the loop.

		while ( have_posts() ) : the_post(); 
      //  $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		?>

        <div class="wrap_crumb_b">
        
	     <ol class="breadcrumb breadcrumb-arrow" style="margin-bottom: 5px;">
         <li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
		<li class="active-d"><?php the_title(); ?></li>
		</ol>
	

</div>

        <?php endwhile; ?>
 


 <div class="page-content">
	<div class="page_wrap">
	<?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>
		<div class="container">
			<div class="saving_pge">
			<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
				<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
				<!-- text -->

				
				<!-- part -->
			</div>
			<!-- page -->
		</div>
  <?php endwhile; ?>

		<div class="additonal_services">
			<div class="wrap_services clearfix">
			<?php
										   $args = array(
										  'post_type' => 'additional_services',
										  
										 'order' =>'ASC'
										  );
										 $the_query = new WP_Query($args); ?>
										  <?php if ( $the_query->have_posts() ) : ?>
										  <?php 
										  $c = 1;
									  while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
									    <?php 
					$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					$link = get_field('links');
                    //print_r($feat_image);die;
					?>
			
				<div class="service_ad">
					<div class="icon_ad">
						<a href="<?php echo $link; ?>"><img src="<?php  echo $feat_image;?>" alt="<?php the_title(); ?>"></a>
					</div>
					<div class="desc_ad">
						<h3>
						<?php the_title(); ?>
						</h3>
						<?php the_content(); ?>
						<?php edit_post_link('<strong><u>Edit</u></strong>'); ?>
					</div>
				</div>
				
				<?php endwhile;endif; ?>
	

				
			</div>
		</div>
		<!-- services -->
	</div>
	<!-- weap -->
</div>


 

<?php get_footer(); ?>