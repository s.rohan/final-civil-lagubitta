<?php
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	// Example of all available fields

    global $wpdb;
    $result = $wpdb->get_results('select * from '.$wpdb->prefix."power_source order by power_source");
    if(!empty($result)){
        foreach($result as $val){
            $option[$val->id] = $val->power_source;
        }
    }

    $result1 = $wpdb->get_results('select * from '.$wpdb->prefix."height order by height");
    if(!empty($result1)){
        foreach($result1 as $val){
            $max_option[$val->id] = $val->height;
        }
    }
			
	$product = array(
		        array( 'id' => 'specification',  'name' => 'Specification', 'type' => 'wysiwyg', 'options' => array( 'editor_height' => '100' )),
                array( 'id' => 'condition',  'name' => 'Condition:', 'type' => 'select','options'=>array('1'=>'Used','2'=>'New') ),
				array( 'id' => 'height',  'name' => 'Max. Working Height (m):', 'type' => 'select','options'=>$max_option ),
				array( 'id' => 'outreach',  'name' => 'Max. Horizontal Outreach (m):', 'type' => 'text' ),
				array( 'id' => 'capacity',  'name' => 'Lift capacity (SWL) (kg):', 'type' => 'text' ),
				array( 'id' => 'travel-width',  'name' => 'Travel Width (m):', 'type' => 'text' ),
				array( 'id' => 'weight',  'name' => 'Weight (kg):', 'type' => 'text' ),
				array( 'id' => 'power-source',  'name' => 'Power Source:', 'type' => 'select','options'=>$option ),
				array( 'id' => 'featured-videos', 'name' => 'Featured Video', 'type' => 'post_select', 'use_ajax' => true, 'query' => array( 'post_type'=>'video' ), 'multiple' => false  ),
				array( 'id' => 'related-videos', 'name' => 'Related Videos', 'type' => 'post_select', 'use_ajax' => true, 'query' => array( 'posts_per_page' => 100,'post_type'=>'video' ), 'multiple' => true  ),
				array( 'id' => 'related-products', 'name' => 'Related Products', 'type' => 'post_select', 'use_ajax' => true, 'query' => array( 'posts_per_page' => 100,'post_type'=>'product' ), 'multiple' => true  ),
				array( 'id' => 'gallery', 'name' => 'Gallery Images', 'type' => 'image',  'repeatable' => true, 'sortable' => 1 ),	
			   array( 'id' => 'downloads', 'name' => 'Upload Files', 'type' => 'file', 'file_type' => array('image','pdf'), 'repeatable' => true, 'sortable' => 1 ),
			
			);
			
	$meta_boxes[] = array(
						'title' => 'Product Details',
						'pages' => 'product',
						'fields' => $product
					);
					
					
			$page_accordion = array(
				array( 'id' => 'tab1',  'name' => 'Tab 1 Heading', 'type' => 'text' ),
				array( 'id' => 'tab1content',  'name' => 'WYSIWYG field', 'type' => 'wysiwyg', 'options' => array( 'editor_height' => '100' ), 'sortable' => true ),
				array( 'id' => 'tab2',  'name' => 'Tab 2 Heading', 'type' => 'text' ),
				array( 'id' => 'tab2content',  'name' => 'WYSIWYG field', 'type' => 'wysiwyg', 'options' => array( 'editor_height' => '100' ),  'sortable' => true ),
				array( 'id' => 'tab3',  'name' => 'Tab 3 Heading', 'type' => 'text' ),
				array( 'id' => 'tab3content',  'name' => 'WYSIWYG field', 'type' => 'wysiwyg', 'options' => array( 'editor_height' => '100' ), 'sortable' => true ),
				
			);
			
	$meta_boxes[] = array(
						'title' => 'Tab Content',
						'pages' => 'page',
						'fields' => $page_accordion
					);			
					
					
		$related_product = array(
			        array( 'id' => 'video-url',  'name' => 'Youtube Video Url', 'type' => 'url' ),
					array( 'id' => 'related-products', 'name' => 'Related Products', 'type' => 'post_select', 'use_ajax' => true, 'query' => array( 'post_type'=>'product','posts_per_page'=>'100' ), 'multiple' => true  ),
					array( 'id' => 'related-videos', 'name' => 'Related Videos', 'type' => 'post_select', 'use_ajax' => true, 'query' => array( 'posts_per_page' => 100,'post_type'=>'video' ), 'multiple' => true  ),
					
			
					
			
			);
			
	$meta_boxes[] = array(
						'title' => 'Video Embed and Products Search',
						'pages' => 'video',
						'fields' => $related_product
					);				
					
					

	return $meta_boxes; 
	
	


}
add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );


