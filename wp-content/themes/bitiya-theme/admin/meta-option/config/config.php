<?php
/**
* Define the metabox and field configurations.
*
* @param  array $meta_boxes
* @return array
*/
function cmb_sample_metaboxes(array $meta_boxes){
	$slider = array(
		array('id' => 'slider-link', 'name' => 'Link', 'type' => 'text'),
	);
    
	$meta_boxes[] = array(
		'title'  => 'Slider Pages',
		'pages'  => 'slider',
		'fields' => $slider,
	);

	/**
	* Banner Image for Individual Post for trip/tour
	*/
	$banner = array(
		array('id' => 'banner-link', 'name' => 'Banner', 'type' => 'image'),
	);

	
    
	$meta_boxes[] = array(
		'title'  => 'Banner Image',
		'pages'  => 'package',
		'fields' => $banner,
	);




	$trip_details = array(
		array('id'=>'trip_name', 'name'=>'Trek Name', 'type'=>'text'),
        array('id'=>'trek_code', 'name'=>'Trek Code', 'type'=>'text'),
		array('id' => 'duration', 'name' => 'Total Days', 'type' => 'text'),
        array('id' => 'trek_day', 'name' => 'Trek Days', 'type' => 'text'),
		array('id' => 'max_alt', 'name' => 'Max Altitude', 'type' => 'text'),
		array('id' => 'meal_plan', 'name' => 'Meal Plan', 'type' => 'text'),
		array('id' => 'mode_of_travel', 'name' => 'Mode of travel', 'type' => 'text'),
		array('id'=>'accommodation','name'=>'Accommodation','type'=>'text'),
		array('id' => 'grade', 'name' => 'Grade', 'type' => 'text'),
		
	);

	$meta_boxes[] = array(
		'title'   => 'Trip Facts',
		'pages'   => 'package',
		'fields'  => $trip_details,
		'context' => 'side',
	);






	/**
	* Add feature trip custom check box
	*/

	$feature_trip = array(

		array('id' => 'feature_trip', 'name' => 'Most admired trips', 'type' => 'checkbox'),
             
	);

	$meta_boxes[] = array(
		'title'   => 'Most Admired Trips',
		'pages'   => 'package',
		'fields'  => $feature_trip,
		'context' => 'side',
	);


	
	/// for client url
 
	$client_link = array(
		array('id' => 'client-link', 'name' => 'Link', 'type' => 'text'),
	);
    
	$meta_boxes[] = array(
		'title'  => 'Cleint  Url',
		'pages'  => 'client_image',
		'fields' => $client_link,
	);

	/**
	* For Google map 
	* Initallized at right side
	*/
	$map = array(

		array('id' => 'latitude', 'name' => 'latitude', 'type' => 'text'),
		array('id' => 'longitude','name' => 'longitude', 'type' => 'text'),
       
	);

	$meta_boxes[] = array(
		'title'   => 'Google Map ',
		'pages'   => 'package',
		'fields'  => $map,
		'context' => 'side',
	);

	/**
	* home page slider link
	*/
	$sliderLink = array(
		// array( 'id' => 'slider-pages', 'name' => 'Pages/Packages', 'type' => 'post_select','repeatable'=>true, 'use_ajax' => true,'query' => array( 'post_type' => array('page','package'),'allow_none'=>true, ), ),
		// array( 'id' => 'slider-package-cat','allow_none'=>true, 'name' => 'Trips Packages', 'type' => 'taxonomy_select',  'taxonomy' => 'activities', 'repeatable'=>true ),
		
		array( 'id' => 'sliderdetails','repeatable'=>true, 'name' => 'Slider Details', 'type' => 'group', 'cols' => 12, 'fields' => array(
			array( 'id' => 'sliderheading',  'name' => 'Enter Slider Title', 'type' => 'text' ),
			array( 'id' => 'sliderdesc',  'name' => 'Enter Slider Description', 'type' => 'wysiwyg','options' => array( 'editor_height' => '320' ) ),
			array( 'id' => 'imagesbasanta', 'name' => 'Image upload field', 'type' => 'image',  'show_size' => true ),
			array('id' => 'home_link', 'name' => 'Link','desc'=>'plz enter the address include(http://)' ,'type' => 'text'),
			array('id' => 'sliderText', 'name' => 'Button Text','desc'=>'Plz enter the slider label', 'type' => 'text')
		) ),
	);
	$meta_boxes[] = array(
		'title'   => 'Slider Options',
		'pages'   => 'slider_gallery',
		'fields'  => $sliderLink,
	);
	


	$trip_details = array(

		array('id' => 'trip-inc-exc', 'name' => 'Trip Inclusion / Exclusion', 'type' => 'wysiwyg', 'options' => array('editor_height' => '300')),
		array('id' => 'general-info', 'name' => 'General Info', 'type' => 'wysiwyg', 'options' => array('editor_height' => '300')),

	);

	$meta_boxes[] = array(
		'title'  => 'Trip Details',
		'pages'  => 'package',
		'fields' => $trip_details,
	);
  
  	$trip_title = array(
	  	array('id' => 'subtitle_title', 'name' => '', 'type' => 'text'),
  	);

	$meta_boxes[] = array(
		'title'  => 'Subtitle',
		'pages'  => 'package',
		'fields' => $trip_title,
	);
  

	
	
	
	$gallery = array(

		array('id' => 'trip-gallery', 'name' => 'Gallery Images', 'type' => 'image', 'repeatable' => true, 'show_size' => true),
	);

	$meta_boxes[] = array(
		'title'  => 'Gallery',
		'pages'  => 'package',
		'fields' => $gallery,
	);

	$di = array(

		array('id' => 'detailed-itinerary', 'name' => '', 'type' => 'group', 'repeatable' => true, 'sortable' => true, 'fields' => array(
				array('id' => 'di-title', 'name' => 'Day', 'type' => 'text'),
				array('id' => 'di-description', 'name' => 'Details', 'type' => 'wysiwyg', 'options' => array('editor_height' => '300')),
				array('id' => 'di-gallery', 'name' => 'Image', 'type' => 'image'),
			)),
	);

	$meta_boxes[] = array(
		'title'  => 'Detailed Itinerary',
		'pages'  => 'package',
		'fields' => $di,
	);


	/** 
	* Add feature trip custom check box
	*/

	$rating = array(

		array(
			'id' => 'rating', 
			'name' => 'Customer Rating', 
			'type' => 'select',
			'placeholder' => 'Select a State',
                'options' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5'
                   
                ),
			),
             
	);
 
	$meta_boxes[] = array(
		'title'   => 'Customer Rating',
		'pages'   => 'testimonials',
		'fields'  => $rating,

	);

	/** 
	* Add videos to the home section
	*/

	$videos = array(
					array('id' => 'videourl', 'name' => 'Video', 'type' => 'text'));
 
	$meta_boxes[] = array(
		'title'   => 'Video URL',
		'pages'   => 'video',
		'fields'  => $videos,

	);
	$shortcode = array(
					array('id' => 'short_code', 'name' => '', 'type' => 'post_select','allow_none'=>true,'query' => array( 
       					  'post_type' => array('slider_gallery')
					)));
 
	$meta_boxes[] = array(
		'title'   => 'Select Slider',
		'pages'   => array('page','post','partner','our_team','video','package'),
		'context' => 'side',
		'fields'  => $shortcode,

	);


	//   echo "<pre>";
	//   print_r ($meta_boxes);
	//   echo "</pre>";
	// exit;
	return $meta_boxes;



}

add_filter('cmb_meta_boxes', 'cmb_sample_metaboxes');
