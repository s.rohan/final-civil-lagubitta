<?php
/**
 * Template Name:  What we offer Template
 *
 */
 get_header();?>
 
  <div class="page-content">
	<div class="page_wrap">
	

		<section class="offers">
	<div class="s_title">
		<h2>What we offer</h2>
		<span class="design">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 85 10.8" style="enable-background:new 0 0 85 10.8;" xml:space="preserve">
				<style type="text/css">
					.st0{fill:#732978;}
					.st2{fill:#095328;}
				</style>
				<g>
					<path class="st0" d="M36.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C39.6,5.9,37.9,7.2,36.2,8.5z"/>
				</g>
				<g>
					<path class="st2" d="M75.2,8.5c-11.3,0-22.7,0-34,0c1.7-1.3,3.3-2.7,5-4c11.3,0,22.7,0,34,0C78.6,5.9,76.9,7.2,75.2,8.5z"/>
				</g>
			</svg>
		</span>
	</div>
	<div class="container">
		<div class="row">
		 <?php
										  $args = array(
										  'post_type' => 'loan',
										  
										 'order' =>'ASC'
										  );
										 $the_query = new WP_Query($args); ?>
										  <?php if ( $the_query->have_posts() ) : ?>
										  <?php 
										  $c = 1;
									  while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
                                       <?php 
					$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                    //print_r($feat_image);die;
					?>
		
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="loan-wrap">
					<div class="loan_icon">
						<img src="<?php echo $feat_image; ?>" alt="">					</div>
					<div class="loan_title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>					</div>
				</div>
			</div>
			
			<?php endwhile;endif; ?>
			<!-- col -->

		
		</div>
	</div>
</section>
		<!-- services -->
	</div>
	<!-- weap -->
</div>
 
 <?php get_footer(); ?>